module.exports = {
	siteMetadata: {
		siteUrl: "https://www.yourdomain.tld",
		title: "Personal website",
	},
	plugins: [
		"gatsby-plugin-gatsby-cloud",
		"gatsby-plugin-styled-components",
		{
			resolve: "gatsby-plugin-react-intl",
			options: {
				path: `${__dirname}/src/locales`,
				languages: ["en", "ru"],
				defaultLanguage: "en",
				fallbackLanguage: "en",
			},
		},
		{
			resolve: "gatsby-plugin-manifest",
			options: {
				icon: "src/images/favicon.svg",
			},
		},
	],
};

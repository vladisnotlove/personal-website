type TContactData = {
	email: string,
	telegram: string,
	gitlab: string,
	github: string
};

export default TContactData;
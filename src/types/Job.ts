type TJob = {
	company: string,
	responsibilities: string,
	usedTechs: string[],
	workResults: {
		displayText: string,
		url: string
	}[],

	startDate: Date | number,
	finishDate?: Date | number,

	unofficially?: boolean,
};

export default TJob;
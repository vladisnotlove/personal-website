type TSkillGroup = {
	groupName: string,
	skills: string[]
}

export default TSkillGroup;
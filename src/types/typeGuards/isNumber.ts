const isNumber = <T>(value: number | T): value is number => {
	return typeof value === "number";
};

export default isNumber;
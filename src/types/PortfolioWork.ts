type TPortfolioWork = {
	name: string,
	preview: string,
	demo?: string,
	gitlab?: string,
	github?: string,
	npm?: string,
	usedTechs: string[],
}

export default TPortfolioWork;
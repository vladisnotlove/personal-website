import React from "react";
import Container from "components/Container";
import Block from "components/Block";
import Experience from "components/Experience";

import { useIntl } from "gatsby-plugin-react-intl";
import useJobs from "data/useJobs";


type ScreenExperienceProps = {
	disableBackground?: boolean,
}

const ScreenExperience: React.FC<ScreenExperienceProps> = (
	{
		disableBackground,
	}
) => {
	const { formatMessage } = useIntl();
	const jobs = useJobs();

	return <Container
		disableBackground={disableBackground}
	>
		<Block
			title={formatMessage({ id: "experienceTitle" })}
		>
			<Experience
				jobs={jobs}
			/>
		</Block>
	</Container>;
};

export default ScreenExperience;
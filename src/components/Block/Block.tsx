import React from "react";
import styled, { css } from "styled-components";
import breakpoint from "styles/breakpoint";
import Text from "components/ui/Text";
import Link from "components/ui/Link";
import classNames from "classnames";

type BlockProps = {
	className?: string,
	title?: React.ReactNode,
	href?: string,
}

const BlockUnstyled: React.FC<BlockProps> = (
	{
		className,
		title,
		href,
		children,
	}
) => {
	return <div className={classNames("block", className)}>
		<Text
			className={"block__title"}
			variant={"h2"}
		>
			{title}
			{href &&
				<Link
					className={"block__hashtag"}
					href={href}
					disableStyles
				>
					#
				</Link>
			}
		</Text>
		<div className={"block__content"}>
			{children}
		</div>
	</div>;
};

const Block = styled(BlockUnstyled)(() => {
	return css`
		&.block {
			padding: 2rem 0;
		}
		
		& .block__title {
			margin-bottom: 2.5rem;
		}
		
		& .block__hashtag {
			line-height: 1em;
			font-size: 0.75em;
			vertical-align: middle;
			margin-left: .5rem;
			opacity: 0.2;
		}
	
		@media screen and ${breakpoint.device.sm} {
			&.block {
				padding: 3rem 0;
			}
		}
	
		@media screen and ${breakpoint.device.md} {
			&.block {
				padding: 4rem 0;
			}
		}
	
		@media screen and ${breakpoint.device.lg} {
			&.block {
				padding: 4rem 0;
			}
		}
	`;
});

export default Block;
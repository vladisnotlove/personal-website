import React, { Fragment } from "react";
import useSkillGroups from "data/useSkillGroups";
import Container from "components/Container";
import Block from "components/Block";
import { useIntl } from "gatsby-plugin-react-intl";
import Text from "components/ui/Text";

const ScreenSkills: React.FC = () => {
	const { formatMessage } = useIntl();
	const skillGroups = useSkillGroups();

	return <Container>
		<Block
			title={formatMessage({ id: "skillsTitle" })}
		>
			<Text fontFamily={"mono"}>
				{skillGroups.map((group, index) => (
					<Fragment key={group.groupName}>
						{index !== 0 && <br />}
						{group.skills.map(skill => (
							<Fragment key={skill}>
								{skill}<br />
							</Fragment>
						))}
					</Fragment>
				))}
			</Text>
		</Block>
	</Container>;
};

export default ScreenSkills;
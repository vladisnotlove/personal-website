import React from "react";
import { useIntl } from "gatsby-plugin-react-intl";
import usePortfolioWorks from "data/usePortfolioWorks";
import Container from "components/Container";
import Block from "components/Block";
import Grid from "components/ui/Grid";
import PortfolioWorkCard from "components/PortfolioWorkCard/PortfolioWorkCard";

const ScreenPortfolio: React.FC = () => {
	const { formatMessage } = useIntl();
	const portfolioWorks = usePortfolioWorks();

	return <Container
		id={"portfolio"}
	>
		<Block
			title={formatMessage({ id: "portfolioTitle" })}
			href={"#portfolio"}
		>
			<Grid
				gapX={3}
				gapY={1}
				columnCount={2}
			>
				{portfolioWorks.map((work, index) => (
					<PortfolioWorkCard
						key={index}
						work={work}
					/>
				))}
			</Grid>
		</Block>
	</Container>;
};

export default ScreenPortfolio;
import React, { forwardRef, useRef, useState } from "react";

import Container from "components/Container";
import Text from "components/ui/Text";
import Button from "components/ui/Button";
import { IconScroll } from "components/ui/icons";

import styled from "styled-components";
import classNames from "classnames";
import { useIntl } from "gatsby-plugin-react-intl";
import breakpoint from "styles/breakpoint";
import useOnPageScroll from "hooks/useOnPageScroll";

type ScreenWelcomeProps = {
	innerRefs?: {
		scrollIcon?: React.Ref<HTMLDivElement>
	}
	className?: string,
	disableBackground?: boolean,
}

const ScreenWelcomeUnstyled = forwardRef<HTMLDivElement, ScreenWelcomeProps>((
	{
		innerRefs,
		className,
		disableBackground,
	},
	ref
) => {
	const [isScrolling, setIsScrolling] = useState(false);
	const intl = useIntl();
	const timeoutIdRef = useRef<number>();

	useOnPageScroll(() => {
		if (timeoutIdRef.current !== undefined) {
			window.clearTimeout(timeoutIdRef.current);
		}
		setIsScrolling(true);
		timeoutIdRef.current = window.setTimeout(() => {
			setIsScrolling(false);
		}, 2000);
	}, []);

	return <div
		ref={ref}
		className={classNames("screenWelcome", className)}
	>
		<Container className={"screenWelcome__container"}
			disableBackground={disableBackground}
		>
			<div className={"screenWelcome__content"}>
				<Text
					className={"screenWelcome__title"}
					variant={"h1"}
				>
					{intl.formatMessage({
						id: "welcomeTitle",
						defaultMessage: "Nikolaev Vladislav"
					})}
				</Text>
				<Text className={"screenWelcome__text"}>
					{intl.formatMessage({
						id: "welcomeText",
						defaultMessage: "React frontend developer from Russia",
					}, {
						br: <br />
					})}
				</Text>
				<a href={"#portfolio"}>
					<Button>
						{intl.formatMessage({
							id: "portfolio",
							defaultMessage: "Portfolio"
						})}
					</Button>
				</a>
			</div>
			<div
				ref={innerRefs?.scrollIcon}
				className={classNames("screenWelcome__iconScrollContainer", {
					["idle"]: !isScrolling
				})}
			>
				<IconScroll />
			</div>

		</Container>
	</div>;
});
ScreenWelcomeUnstyled.displayName = "ScreenWelcomeUnstyled";

const ScreenWelcome = styled(ScreenWelcomeUnstyled)`
	&.screenWelcome {
		display: flex;
		flex-direction: column;
		min-height: 100vh;
	}
	& .screenWelcome__container {
		flex-grow: 1;
		display: flex;
		flex-direction: column;
		justify-content: center;
	}
	& .screenWelcome__content {
		padding-top: 5rem;
		padding-bottom: 3rem;
	}
	& .screenWelcome__title {
		margin-bottom: 1rem;
	}
	& .screenWelcome__text {
		margin-bottom: 2rem;
	}
	
	@keyframes scrollIconIdle {
		0% {
			transform: translateY(0);
		}
		80% {
			transform: translateY(0);
		}
		90% {
			transform: translateY(1rem);
			animation-timing-function: ease-in;
		}
		100% {
			transform: translateY(0);
		}
	}
	
	& .screenWelcome__iconScrollContainer {
		position: fixed;
		bottom: 2rem;
		left: 50%;
		stroke-width: 2;
	}
	& .screenWelcome__iconScrollContainer > * {
		transform: translateX(-50%);
	}
	
	& .screenWelcome__iconScrollContainer.idle {
		animation: scrollIconIdle 3s infinite;
	}

	@media screen and ${breakpoint.device.sm} {
		& .screenWelcome__content {
			padding-top: 6rem;
			padding-bottom: 3rem;
		}
		& .screenWelcome__iconScrollContainer {
			left: initial;
		}
		
		& .screenWelcome__iconScrollContainer > * {
			transform: translateX(0);
		}
	}
`;

export default ScreenWelcome;



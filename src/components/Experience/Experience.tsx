import React, { Fragment } from "react";
import styled, { css } from "styled-components";
import TJob from "types/Job";
import { useIntl } from "react-intl";
import useFormatDate from "hooks/useFormatDate";
import classNames from "classnames";
import Text from "components/ui/Text";
import Link from "components/ui/Link";
import breakpoint from "styles/breakpoint";


type ExperienceProps = {
	className?: string,
	jobs: TJob[]
}

const ExperienceUnstyled: React.FC<ExperienceProps> = (
	{
		className,
		jobs,
	}
) => {
	const { formatMessage } = useIntl();
	const {
		formatDate,
		formatDistance
	} = useFormatDate();

	return <div
		className={classNames("experience", className)}
	>
		{jobs.map((
			{
				company,
				responsibilities,
				usedTechs,
				workResults,
				startDate,
				finishDate,
				unofficially,
			},
			index
		) => {
			const startDateFormatted = formatDate(startDate, "yyyy MMMM");
			const finishDateFormatted = finishDate ?
				formatDate(finishDate, "yyyy MMMM") :
				formatMessage({ id: "now" });

			return <div
				key={company + "-" + index}
				className={"job"}
			>
				<div className={"job__main"}>
					<Text
						className={"job__dateDistance"}
						color={"textAccent"}
					>
						{formatDistance(startDate, finishDate || new Date())}
					</Text>
					<div className={"job__company"}>
						{company}
						{unofficially && <Text
							className={"job__unofficiallyLabel"}
							display={"inline"}
							color={"textSecondary"}
						>
							({formatMessage({ id: "unofficially" })})
						</Text>}
					</div>
					<div className={"job__dateDuration"}>
						{startDateFormatted} – {finishDateFormatted}
					</div>
				</div>

				<div className={"job__details"}>

					{/* Responsibilities */}
					<div className={"job__detailTitle"}>
						{formatMessage({ id: "responsibilities" })}:
					</div>
					<div className={"job__detailBody"}>
						{responsibilities}
					</div>

					{/* Used techs */}
					<div className={"job__detailTitle"}>
						{formatMessage({ id: "techStack" })}:
					</div>
					<div className={"job__detailBody"}>
						{usedTechs.join(", ")}
					</div>

					{/* Work results */}
					<div className={"job__detailTitle"}>
						{formatMessage({ id: "workResults" })}:
					</div>
					<div className={"job__detailBody"}>
						{workResults.map((
							{
								displayText,
								url,
							},
							index
						) => (
							<Fragment key={url}>
								{index !== 0 && ", "}
								<Link
									href={url}
									target={"_blank"}
								>
									{displayText}
								</Link>
							</Fragment>
						))}
					</div>
				</div>
			</div>;
		})}
	</div>;
};

const Experience = styled(ExperienceUnstyled)((
	{
		theme,
	}
) => {
	return css`
		& .job:not(:last-child) {
			margin-bottom: 2rem;
		}
		
		& .job__main {
			margin-bottom: 1rem;
			line-height: 2em;
		}
		& .job__details {
			background: ${theme.palette.base.dark};
			padding: 1rem 1.5rem;
			border-radius: .5rem;
			max-width: ${breakpoint.size.xs}px;
		}
		
		& .job__dateDuration {
			font-style: italic;
			text-transform: lowercase;
		}
		& .job__unofficiallyLabel {
			margin-left: .25rem;
			text-transform: lowercase;
			font-style: italic;
		}
		& .job__detailTitle {
			margin-bottom: .25rem;
			line-height: 1.25em;
			font-style: italic;
			color: ${theme.palette.text.secondary};
		}
		& .job__detailBody {
			margin-bottom: 1rem;
			line-height: 1.5em;
		}
		& .job__detailBody:last-child {
			margin-bottom: 0;
		}
	`;
});

export default Experience;
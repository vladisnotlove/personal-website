import React from "react";
import styled, { css } from "styled-components";
import classNames from "classnames";
import breakpoint from "styles/breakpoint";

type ContainerProps = {
	className?: string,
	disableBackground?: boolean,
	id?: string,
}

const ContainerUnstyled: React.FC<ContainerProps> = (
	{
		className,
		children,
		id,
	}
) => {
	return <div
		className={classNames("container", className)}
		id={id}
	>
		{children}
	</div>;
};

const Container = styled(ContainerUnstyled)((
	{
		theme,
		disableBackground,
	}
) => css`
	&.container {
		padding: 0 2rem;
		color: ${theme.palette.text.primary};
		background: ${!disableBackground ? theme.palette.base.main : "transparent"};
	}

	@media screen and ${breakpoint.device.sm} {
		&.container {
			padding: 0 calc(50vw - (${breakpoint.size.sm}px * 0.5 - 2rem));
		}
	}

	@media screen and ${breakpoint.device.md} {
		&.container {
			padding: 0 calc(50vw - (${breakpoint.size.md}px * 0.5 - 6rem));
		}
	}

	@media screen and ${breakpoint.device.lg} {
		&.container {
			padding: 0 calc(50vw - (${breakpoint.size.lg}px * 0.5 - 10rem));
		}
	}
`);

export default Container;
import React from "react";
import styled, { css } from "styled-components";
import classNames from "classnames";
import TPortfolioWork from "types/PortfolioWork";
import Link from "components/ui/Link";
import Grid from "components/ui/Grid";
import { IconGithub, IconGitlab, IconNpm } from "components/ui/icons";

type PortfolioWorkCardProps = {
    className?: string,
	work: TPortfolioWork,
}

const PortfolioWorkCardUnstyled: React.FC<PortfolioWorkCardProps> = (
	{
		className,
		work: {
			name,
			preview,
			gitlab,
			github,
			npm,
			demo,
			usedTechs,
		},
	}
) => {

	return <div
		className={classNames(className, "portfolioWorkCard")}
	>
		<Link
			className={"portfolioWorkCard__preview"}
			href={demo}
			target={"_blank"}
			disableStyles
		>
			<div className={"portfolioWorkCard__previewImgContainer"}>
				<img
					className={"portfolioWorkCard__previewImg"}
					src={preview}
					alt={preview}
				/>
			</div>
		</Link>
		<div className={"portfolioWorkCard__main"}>
			<Link
				className={"portfolioWorkCard__title"}
				href={demo}
				target={"_blank"}
				disableStyles
			>
				{name}
			</Link>
			<span>
				{gitlab &&
					<Link
						className={"portfolioWorkCard__link"}
						href={gitlab}
						target={"_blank"}
						title={gitlab}
					>
						<IconGitlab className={"portfolioWorkCard__linkIcon"} />
					</Link>
				}
				{github &&
					<Link
						className={"portfolioWorkCard__link"}
						href={github}
						target={"_blank"}
						title={github}
					>
						<IconGithub className={"portfolioWorkCard__linkIcon"} />
					</Link>
				}
				{npm &&
					<Link
						className={"portfolioWorkCard__link"}
						href={npm}
						target={"_blank"}
						title={npm}
					>
						<IconNpm className={"portfolioWorkCard__linkIcon"} />
					</Link>
				}
			</span>
		</div>
		<Grid
			gapX={0.75}
			gapY={0.75}
		>
			{usedTechs.map(tech => (
				<span
					key={tech}
					className={"portfolioWorkCard__usedTech"}
				>
					{tech}
				</span>
			))}
		</Grid>
	</div>;
};

const PortfolioWorkCard = styled(PortfolioWorkCardUnstyled)((
	{
		theme,
		work: {
			demo
		}
	}
) => {
	return css`
		&.portfolioWorkCard {

		}

		& .portfolioWorkCard__preview {
			display: block;
			position: relative;
			padding-bottom: 56.25%;
			background: ${theme.palette.gray.lighter};
			border-radius: .5rem;
			margin-bottom: 1rem;

			.portfolioWorkCard__previewImgContainer {
				position: absolute;
				top: 0.25rem;
				left: 0.25rem;
				width: calc(100% - 0.5rem);
				height: calc(100% - 0.5rem);
				border-radius: 2rem;
				transition: border-radius ${theme.animation.slow};
				overflow: hidden;
			}

			.portfolioWorkCard__previewImg {
				width: 100%;
				height: 100%;
				object-fit: cover;
				transform: scale(1, 1);
				transition: transform ${theme.animation.slow};
			}

			${demo && css`

				:hover {
					transition: background ${theme.animation.slow};
					cursor: pointer;

					.portfolioWorkCard__previewImgContainer {
						border-radius: .5rem;
						transition: border-radius ${theme.animation.slow};
					}

					.portfolioWorkCard__previewImg {
						transform: scale(1.1, 1.1);
						transition: transform ${theme.animation.slow};
					}
				}

			`}
		}

		& .portfolioWorkCard__main {
			display: flex;
			justify-content: space-between;
			margin-bottom: 1rem;
		}

		& .portfolioWorkCard__title {
			line-height: 1.5em;

			${demo && css`
				color: ${theme.palette.text.accent};

				:hover {
					cursor: pointer;
					text-decoration: underline;
				}
			`}
		}

		& .portfolioWorkCard__link {
			display: inline-block;
			padding: .25rem;
			border-radius: 50%;
			transition: background ${theme.animation.slow};
			line-height: 0;
			
			:hover {
				background: ${theme.palette.action.hover};
				transition: background ${theme.animation.fast};
			}
		}

		& .portfolioWorkCard__usedTech {
			color: ${theme.palette.text.secondary};
		}
	`;
});

export default PortfolioWorkCard;
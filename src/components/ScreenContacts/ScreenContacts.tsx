import React from "react";
import styled, { css } from "styled-components";

import Container from "components/Container";
import Block from "components/Block";
import Link from "components/ui/Link";
import { IconGithub, IconGitlab, IconTelegram } from "components/ui/icons";

import { useIntl } from "gatsby-plugin-react-intl";
import useContactData from "data/useContactData";


type ScreenContactsProps = {
	className?: string,
}

const ScreenContactsUnstyled: React.FC<ScreenContactsProps> = (
	{
		className
	}
) => {
	const { formatMessage } = useIntl();
	const {
		github,
		gitlab,
		telegram,
		email
	} = useContactData();

	return <Container className={className}>
		<Block
			title={formatMessage({ id: "contactsTitle" })}
		>
			{email &&
				<Link
					className={"screenContacts__email"}
					href={`mailto:${email}`}
					disableStyles
				>
					{email}
				</Link>
			}
			<div>
				{telegram &&
					<Link
						className={"screenContacts__iconLink"}
						href={telegram}
						target={"_blank"}
						disableStyles
					>
						<IconTelegram fontSize={"inherit"} />
					</Link>
				}
				{github &&
					<Link
						className={"screenContacts__iconLink"}
						href={github}
						target={"_blank"}
						disableStyles
					>
						<IconGithub fontSize={"inherit"} />
					</Link>
				}
				{gitlab &&
					<Link
						className={"screenContacts__iconLink"}
						href={gitlab}
						target={"_blank"}
						disableStyles
					>
						<IconGitlab fontSize={"inherit"} />
					</Link>
				}
			</div>
		</Block>
	</Container>;
};

const ScreenContacts = styled(ScreenContactsUnstyled)(() => {
	return css`
		& .screenContacts__email {
			display: block;
			margin-bottom: 1.5rem;
			
			:hover {
				cursor: pointer;
			}
		}
		
		& .screenContacts__iconLink {
			font-size: 2rem;
			
			:not(:last-child) {
				margin-right: 1rem;
			}
		}
	`;
});

export default ScreenContacts;

import React, { forwardRef } from "react";
import styled, { css } from "styled-components";
import classNames from "classnames";
import breakpoint from "styles/breakpoint";
import LanguageSwitcher from "components/LanguageSwitcher";
import { useIntl } from "react-intl";

type HeaderProps = {
	className?: string,
	position?: "fixed" | "absolute"
}

const HeaderUnstyled = forwardRef<HTMLDivElement, HeaderProps>((
	{
		className
	},
	ref
) => {
	const intl = useIntl();

	return <div
		ref={ref}
		className={classNames("header", className)}
	>
		<nav className={"header__nav"}>
			<div className={"header__logo"}>
				{intl.formatMessage({
					id: "logo",
					defaultMessage: "N.Vlad"
				})}
			</div>
		</nav>
		<LanguageSwitcher
			className={"header__languageSwitcher"}
		/>
	</div>;
});
HeaderUnstyled.displayName = "HeaderUnstyled";

const Header = styled(HeaderUnstyled)((
	{
		theme,
		position
	}
) => {
	return css`
		&.header {
			position: ${position || "relative"};
			top: 0;
			left: 0;
			width: 100%;
			
			display: flex;
			flex-direction: row;
			justify-content: space-between;
			padding: 1rem;
			color: ${theme.palette.text.primary};
		}
		
		& .header__logo {
			font-size: 20px;
		}
		
		@media screen and ${breakpoint.device.sm} {
			&.header {
				padding: 2rem 4rem;
			}
		}

		@media screen and ${breakpoint.device.md} {
			&.header {
				padding: 2rem 6rem;
			}
		}

		@media screen and ${breakpoint.device.lg} {
			&.header {
				padding: 2rem 8rem;
			}
		}
	`;
});

export default Header;
import React, { useState } from "react";
import styled, { css } from "styled-components";
import classNames from "classnames";
import { changeLocale, useIntl } from "gatsby-plugin-react-intl";
import usePageScrollRestoration from "hooks/usePageScrollRestoration";


type LanguageSwitcherProps = {
    className?: string
}

const LanguageSwitcherUnstyled: React.FC<LanguageSwitcherProps> = (
	{
		className
	}
) => {
	const { locale } = useIntl();
	const [language, setLanguage] = useState(locale);

	const { saveScroll } = usePageScrollRestoration();

	return <div
		className={classNames("languageSwitcher", className)}
		onClick={() => {
			const next = language === "ru" ? "en" : "ru";
			setLanguage(next);
			changeLocale(next);
			saveScroll(window.scrollY);
		}}
	>
		<span
			className={classNames("languageSwitcher__language", {
				"selected": language === "ru"
			})}
		>
			ru
		</span>
		<span className={"languageSwitcher__divider"}>
			|
		</span>
		<span
			className={classNames("languageSwitcher__language", {
				"selected": language === "en"
			})}
		>
			en
		</span>
	</div>;
};

const LanguageSwitcher = styled(LanguageSwitcherUnstyled)((
	{
		theme,
	}
) => {
	return css`
		&.languageSwitcher {
			cursor: pointer;
			user-select: none;
			
			.languageSwitcher__language {
				display: inline-block;
				position: relative;
				color: ${theme.palette.text.secondary};
				transform: scale(1, 1);
				transition: color ${theme.animation.normal};

				&.selected {
					color: ${theme.palette.text.primary};
				}
			}
			
			.languageSwitcher__divider {
				margin: 0 1rem;
			}
			
			&:hover {
				.languageSwitcher__language:not(.selected) {
					text-decoration: underline;
				}
			}
		}
	`;
});

export default LanguageSwitcher;
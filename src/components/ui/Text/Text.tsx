import React from "react";
import styled, { css } from "styled-components";
import classNames from "classnames";

export type TextProps = {
	className?: string,
	variant?: "h1" | "h2" | "p",
	color?: "primary" | "textPrimary" | "textSecondary" | "textAccent" | "inherit",
	display?: "inline" | "inline-block" | "block",
	fontFamily?: "mono" | "sans" | "default"
}

const TextUnstyled: React.FC<TextProps> = (
	{
		className,
		variant = "p",
		children
	}
) => {
	const Tag = variant;

	return <Tag className={classNames("text", className)}>
		{children}
	</Tag>;
};

const Text = styled(TextUnstyled)((
	{
		theme,
		color = "textPrimary",
		display,
		fontFamily = "sans"
	}
) => {
	const colorMapper: Record<typeof color, string | undefined> = {
		primary: theme.palette.primary.main,
		textPrimary: theme.palette.text.primary,
		textSecondary: theme.palette.text.secondary,
		textAccent: theme.palette.text.accent,
		inherit: "inherit"
	};

	const fontFamilyMapper: Record<typeof fontFamily, string | undefined> = {
		sans: theme.typography.fonts.sans,
		mono: theme.typography.fonts.mono,
		default: undefined,
	};

	return css`
		&.text {
			display: ${display};
			color: ${colorMapper[color]};
		}
		
		p.text& {
			font-family: ${theme.typography.base.fontFamily};
			font-size: ${theme.typography.base.fontSize};
			font-weight: ${theme.typography.base.fontWeight};
			font-style: ${theme.typography.base.fontStyle};
			letter-spacing: ${theme.typography.base.letterSpacing};
			line-height: ${theme.typography.base.lineHeight};
		}
		h1.text& {
			font-family: ${theme.typography.h1.fontFamily};
			font-size: ${theme.typography.h1.fontSize};
			font-weight: ${theme.typography.h1.fontWeight};
			font-style: ${theme.typography.h1.fontStyle};
			letter-spacing: ${theme.typography.h1.letterSpacing};
			line-height: ${theme.typography.h1.lineHeight};
		}
		h2.text& {
			font-family: ${theme.typography.h2.fontFamily};
			font-size: ${theme.typography.h2.fontSize};
			font-weight: ${theme.typography.h2.fontWeight};
			font-style: ${theme.typography.h2.fontStyle};
			letter-spacing: ${theme.typography.h2.letterSpacing};
			line-height: ${theme.typography.h2.lineHeight};
		}
		
		${fontFamily && fontFamily !== "default" && css`
			&.text {
				font-family: ${fontFamilyMapper[fontFamily]};
			}
		`}
	`;
});

export default Text;
import React, { AnchorHTMLAttributes } from "react";
import styled, { css } from "styled-components";
import classNames from "classnames";

type LinkProps = {
	className?: string,
	disableStyles?: boolean,

} & Pick<AnchorHTMLAttributes<HTMLElement>, "href" | "target" | "download" | "title">

const LinkUnstyled: React.FC<LinkProps> = (
	{
		className,
		children,
		// eslint-disable-next-line @typescript-eslint/no-unused-vars
		disableStyles,
		...props
	}
) => {
	return <a
		className={classNames("link", className)}
		{...props}
	>
		{children}
	</a>;
};

const Link = styled(LinkUnstyled)((
	{
		theme,
		disableStyles,
	}
) => {
	return css`
		${!disableStyles && css`
			color: ${theme.palette.primary.main};
			text-decoration: underline;

			&:hover {
				text-decoration: none;
			}
		`}

		${disableStyles && css`
			color: inherit;
			text-decoration: none;
		`}
	`;
});

export default Link;
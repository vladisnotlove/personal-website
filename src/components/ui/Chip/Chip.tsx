import React from "react";
import styled, { css } from "styled-components";
import classNames from "classnames";


type ChipProps = {
    className?: string,
}

const ChipUnstyled: React.FC<ChipProps> = (
	{
		className,
		children,
	}
) => {
	return <span
		className={classNames(className, "chip")}
	>
		{children}
	</span>;
};

const Chip = styled(ChipUnstyled)((
	{
		theme
	}
) => {
	return css`
        &.chip {
	        display: inline-block;
			border-radius: 1rem;
	        padding: .375rem .75rem;
	        font-size: .875rem;
	        line-height: 1em;
            background: ${theme.palette.base.dark};
	        color: ${theme.palette.text.primary};
        }
    `;
});

export default Chip;
import React from "react";
import styled, { css } from "styled-components";
import classNames from "classnames";

// TYPES

export type IconProps = {
	className?: string,
	fontSize?: "inherit" | "medium" | "large";
}

// COMPONENT

const IconUnstyled: React.FC<IconProps> = (
	{
		className,
		children
	}
) => {
	return <svg viewBox={"0 0 24 24"} className={classNames("icon", className)}>
		{children}
	</svg>;
};

// STYLES

const fontSizeToPx: Record<Required<IconProps>["fontSize"], string> = {
	inherit: "inherit",
	medium: "18px",
	large: "22px"
};

const Icon = styled(IconUnstyled)((
	{
		fontSize = "medium"
	}
) => {
	return css`
		&.icon {
			font-size: ${fontSizeToPx[fontSize]};
			width: 1em;
			height: 1em;
			color: inherit;
			fill: currentColor;
			display: inline-block;
		}
	`;
});

export default Icon;
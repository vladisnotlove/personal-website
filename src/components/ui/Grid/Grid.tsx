import React, { Children } from "react";
import styled, { css } from "styled-components";
import classNames from "classnames";


type GridProps = {
    className?: string,
	gapX?: number,
	gapY?: number,
	columnCount?: number,
}

const GridUnstyled: React.FC<GridProps> = (
	{
		className,
		children,
	}
) => {
	return <div
		className={classNames(className, "grid")}
	>
		<div className={"grid__inner"}>
			{Children.map(children, (child, index) => (
				<div
					key={index}
					className={"grid__cell"}
				>
					{child}
				</div>
			))}
		</div>
	</div>;
};

const Grid = styled(GridUnstyled)((
	{
		gapX = 0,
		gapY = 0,
		columnCount,
	}
) => {
	const validColumnCount = columnCount !== undefined ?
		Math.max(1, Math.round(Math.abs(columnCount))) :
		undefined;

	return css`
        &.grid {
        }
		
		& > .grid__inner {
			display: flex;
			flex-wrap: wrap;
			margin: -${gapY * 0.5}rem -${gapX * 0.5}rem;
		}
		
		& > .grid__inner > .grid__cell {
			${validColumnCount && css`
				flex-grow: 1;
				width: ${100 / validColumnCount}%;
				max-width: ${100 / validColumnCount}%;
			`}
			padding: ${gapY * 0.5}rem ${gapX * 0.5}rem;
		}
    `;
});

export default Grid;
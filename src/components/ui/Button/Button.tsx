import styled, { css } from "styled-components";


const Button = styled.button(({
	theme,
}) => {
	return css`
		padding: .875rem 1.375rem;
		border: none;
		border-radius: .75rem;
		font-weight: 500;
		letter-spacing: .05em;
		line-height: 1.25em;
		background: ${theme.palette.primary.main};
		color: ${theme.palette.primary.contrastText};
		transition: all ${theme.animation.normal};
		
		&:hover {
			background: ${theme.palette.primary.dark};
			transition: all ${theme.animation.fast};
		}
	`;
});

export default Button;
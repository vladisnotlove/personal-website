import TJob from "types/Job";
import { useIntl } from "react-intl";


const useJobs = (): TJob[] => {
	const { formatMessage } = useIntl();
	return [
		{
			company: formatMessage({ id: "companyNordInteractive" }),
			responsibilities: formatMessage({ id: "companyNordInteractiveResponsibilities" }),
			usedTechs: ["js", "css", "html"],
			workResults: [
				{
					url: "https://gitlab.com/vladisnotlove/tribune-landing-for-rustem",
					displayText: "tribune-landing-repo"
				}
			],

			startDate: new Date(2019, 9, 1),
			finishDate: new Date(2020, 1, 20),

			unofficially: true,
		},
		{
			company: formatMessage({ id: "companyA201" }),
			responsibilities: formatMessage({ id: "companyA201Responsibilities" }),
			usedTechs: ["Typescript", "SASS", "Next.js", "React", "React-query", "Zustand", "React-hook-form"],
			workResults: [
				{
					url: "https://agggro.com",
					displayText: "agggro.com"
				}
			],

			startDate: new Date(2020, 7, 20),
			finishDate: new Date(2023, 0, 9),
		}
	];

};

export default useJobs;

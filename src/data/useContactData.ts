import TContactData from "types/ContactData";

const useContactData = (): TContactData => {
	return {
		email: "vlad.nikolaev.p@gmail.com",
		telegram: "https://t.me/vladisnotlove",
		gitlab: "https://gitlab.com/vladisnotlove",
		github: "https://github.com/vladisnotlove"
	};
};

export default useContactData;
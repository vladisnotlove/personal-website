import { useIntl } from "react-intl";
import TSkillGroup from "types/SkillGroup";

const useSkillGroups = (): TSkillGroup[] => {
	const { formatMessage } = useIntl();

	return [
		{
			groupName: "React",
			skills: [
				"React",
				"React-query",
				"React-hook-form",
				"Material-UI",
				"Next.js",
				"Styled-components",
				"Gatsby"
			]
		},
		{
			groupName: "JavaScript",
			skills: [
				"Javascript",
				"Typescript",
			]
		},
		{
			groupName: "CSS",
			skills: [
				"CSS",
				"SASS",
				formatMessage({ id: "adaptiveDesign" }),
			]
		},
		{
			groupName: "Other",
			skills: [
				"Git",
				formatMessage({ id: "english" }),
			]
		}
	];
};

export default useSkillGroups;

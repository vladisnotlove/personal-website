import TPortfolioWork from "types/PortfolioWork";
import agggroComPreview from "images/agggro-com-preview.png";
import personalWebsitePreview from "images/personal-website-preview.png";
import { useIntl } from "react-intl";

const usePortfolioWorks = (): TPortfolioWork[] => {
	const { formatMessage } = useIntl();

	return [
		{
			name: "agggro.com",
			preview: agggroComPreview,
			demo: "https://agggro.com",
			usedTechs: ["Typescript", "SASS", "ESLint", "Next.js", "React", "React-query", "Zustand", "React-hook-form", "Yandex maps", "next-i18next"],
		},
		{
			name: formatMessage({ id: "personalWebsite" }),
			preview: personalWebsitePreview,
			demo: "https://gitlab.com/vladisnotlove/personal-website", // todo: change after deploy
			gitlab: "https://gitlab.com/vladisnotlove/personal-website",
			usedTechs: ["Typescript", "ESLint", "Gatsby", "React", "Styled-components", "formatJS"],
		}
	];
};

export default usePortfolioWorks;
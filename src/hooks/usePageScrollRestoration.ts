import { useEffect } from "react";

type UsePageScrollRestorationReturn = {
	saveScroll: (scrollY: number) => void
}

const sessionState = {
	prevScroll: 0,
	scrollSaved: false,
};

const saveScroll = (scrollY: number) => {
	sessionState.prevScroll = scrollY;
	sessionState.scrollSaved = true;
};

/**
 * Restore scrollY of page on mount
 */
const usePageScrollRestoration = (): UsePageScrollRestorationReturn => {

	useEffect(() => {
		if (sessionState.scrollSaved) {
			window.scroll({
				top: sessionState.prevScroll,
			});
			sessionState.scrollSaved = false;
		}
	}, []);

	return {
		saveScroll
	};
};

export default usePageScrollRestoration;
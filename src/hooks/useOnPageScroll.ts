import { useEffect, useRef } from "react";

type TPageScrollHandler = (
	data: {
		scrollPosition: {
			x: number,
			y: number,
		}
		scrollVelocity?: {
			x: number,
			y: number,
		}
	},
	e: Event
) => void

const useOnPageScroll = (pageScrollHandler: TPageScrollHandler, deps: unknown[]): void => {
	const prevScrollPositionRef = useRef<{x?: number, y?: number}>({});
	const prevTimeRef = useRef<number>();

	useEffect(() => {
		prevScrollPositionRef.current = {
			x: window.scrollX,
			y: window.scrollY
		};
		prevTimeRef.current = Date.now();
		const listener = (e: Event) => {
			const prevScrollX = prevScrollPositionRef.current.x as number;
			const prevScrollY = prevScrollPositionRef.current.y as number;
			const prevTime = prevTimeRef.current as number;

			// calc data for handler
			const deltaTime = Date.now() - prevTime;
			const scrollPosition = {
				x: window.scrollX,
				y: window.scrollY
			};
			const scrollVelocity = {
				x: (window.scrollX - prevScrollX) / deltaTime,
				y: (window.scrollY - prevScrollY) / deltaTime
			};

			// set prev data
			prevScrollPositionRef.current = {
				x: window.scrollX,
				y: window.scrollY
			};
			prevTimeRef.current = Date.now();

			// call handler
			pageScrollHandler({
				scrollPosition,
				scrollVelocity
			}, e);
		};
		window.addEventListener("scroll", listener);
		return () => window.removeEventListener("scroll", listener);
	}, deps);
};

export default useOnPageScroll;
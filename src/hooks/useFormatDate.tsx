import { useIntl } from "react-intl";
import {
	format as dformat,
	formatDistance as dformatDistance,
} from "date-fns";

// import languages
import ru from "date-fns/locale/ru";
import en from "date-fns/locale/en-US";


const intlLocaleToDateLocale: Record<string, Locale | undefined> = {
	["ru"]: ru,
	["en"]: en,
};

const useFormatDate = () => {
	const intl = useIntl();

	const formatDate: typeof dformat = (date, format, options = {}) => {
		options.locale = intlLocaleToDateLocale[intl.locale];
		return dformat(date, format, options);
	};

	const formatDistance: typeof dformatDistance = (date, baseDate, options = {}) => {
		options.locale = intlLocaleToDateLocale[intl.locale];
		return dformatDistance(date, baseDate, options);
	};

	return {
		formatDate,
		formatDistance
	};
};

export default useFormatDate;
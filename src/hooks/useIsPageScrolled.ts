import { useEffect, useState } from "react";
import isNumber from "types/typeGuards/isNumber";
import useOnPageScroll from "hooks/useOnPageScroll";

type UseIsPageScrolledProps = {
	minScrollY?: number | (() => number),
	maxScrollY?: number | (() => number),
}

const useIsPageScrolled = (
	{
		minScrollY = Number.NEGATIVE_INFINITY,
		maxScrollY = Number.POSITIVE_INFINITY
	}: UseIsPageScrolledProps
): boolean | undefined => {
	const [isPageScrolled, setIsPageScrolled] = useState<boolean>();

	const check = () => {
		const _minScrollY = isNumber(minScrollY) ? minScrollY : minScrollY();
		const _maxScrollY = isNumber(maxScrollY) ? maxScrollY : maxScrollY();
		if (window.scrollY > _minScrollY && window.scrollX < _maxScrollY) {
			setIsPageScrolled(true);
		}
		else {
			setIsPageScrolled(false);
		}
	};

	useOnPageScroll(check, [
		minScrollY,
		maxScrollY
	]);

	useEffect(check, [
		minScrollY,
		maxScrollY
	]);

	return isPageScrolled;
};

export default useIsPageScrolled;
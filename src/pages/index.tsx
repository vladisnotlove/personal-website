import React, { useCallback, useRef } from "react";

import App from "./_app";
import ScreenWelcome from "components/ScreenWelcome";
import ScreenExperience from "components/ScreenExperience";
import ScreenSkills from "components/ScreenSkills";
import ScreenPortfolio from "components/ScreenPortfolio";
import ScreenContacts from "components/ScreenContacts";
import Header from "components/Header";
import WaterAnimation, { useWaterAnimationManager } from "react-water-animation-2d";

import theme, { brightTheme } from "styles/theme";
import styled, { css, ThemeProvider } from "styled-components";
import useIsPageScrolled from "hooks/useIsPageScrolled";
import useOnPageScroll from "hooks/useOnPageScroll";


const IndexPage: React.FC = () => {
	const headerRef = useRef<HTMLDivElement>(null);
	const screenWelcomeRef = useRef<HTMLDivElement>(null);
	const scrollIconRef = useRef<HTMLDivElement>(null);

	const minScrollY = useCallback(() => {
		if (!headerRef.current || !screenWelcomeRef.current) return Number.POSITIVE_INFINITY;
		return screenWelcomeRef.current.clientHeight - headerRef.current.clientHeight * 0.5;
	}, []);
	const isPageScrolled = useIsPageScrolled({
		minScrollY: minScrollY,
	});

	const {
		control,
		waterAnimationManager
	} = useWaterAnimationManager();

	const prevIsUnderWaterRef = useRef<boolean>();
	useOnPageScroll(({ scrollVelocity }) => {
		if (isPageScrolled) return;
		if (!scrollIconRef.current) return;
		if (!screenWelcomeRef.current) return;
		if (!waterAnimationManager) return;
		if (!scrollVelocity) return;

		const scrollIconRect = scrollIconRef.current.getBoundingClientRect();
		const screenWelcomeRect = screenWelcomeRef.current.getBoundingClientRect();
		const scrollIconPosition = {
			x: scrollIconRect.left - screenWelcomeRect.left,
			y: scrollIconRect.top - screenWelcomeRect.top,
		};

		const prevIsUnderWater = prevIsUnderWaterRef.current;
		const isUnderWater = waterAnimationManager.isUnderSurface(scrollIconPosition.x, scrollIconPosition.y);

		if (prevIsUnderWater === undefined) {
			prevIsUnderWaterRef.current = isUnderWater;
			return;
		}

		if (!prevIsUnderWater && isUnderWater) {
			waterAnimationManager.applyForce(
				scrollIconPosition.x + scrollIconRect.width * 0.5,
				{
					x: 0,
					y: Math.min(scrollVelocity.y * 1000, 5000)
				},
				200
			);
		}

		prevIsUnderWaterRef.current = isUnderWater;
	}, [
		waterAnimationManager,
		isPageScrolled
	]);

	return <App>
		<ThemeProvider theme={isPageScrolled ? theme : brightTheme}>
			<StyledHeader
				key={isPageScrolled ? "default" : "bright"}
				ref={headerRef}
				position={"fixed"}
			/>
		</ThemeProvider>
		<div
			style={{
				position: "relative"
			}}
		>
			<WaterAnimationBackground
				control={control}
				upperColor={brightTheme.palette.base.main}
				bottomColor={theme.palette.base.main}
			/>
			<ThemeProvider theme={brightTheme}>
				<ScreenWelcome
					ref={screenWelcomeRef}
					innerRefs={{
						scrollIcon: scrollIconRef
					}}
					disableBackground
				/>
			</ThemeProvider>
		</div>
		<ScreenExperience
			disableBackground
		/>
		<ScreenSkills />
		<ScreenPortfolio />
		<ScreenContacts />
	</App>;
};

const StyledHeader = styled(Header)(() => css`
	z-index: 10;
`);

const WaterAnimationBackground = styled(WaterAnimation)(() => css`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 200%;
	z-index: -1;
	overflow: visible;
`);

export default IndexPage;

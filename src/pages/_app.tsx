import React from "react";
import { ThemeProvider } from "styled-components";
import theme from "styles/theme";
import GlobalStyle from "styles/global";

import "styles/fonts.css";
import "styles/reset.css";

const App: React.FC = (
	{
		children
	}
) => {
	return <ThemeProvider theme={theme}>
		{children}
		<GlobalStyle />
	</ThemeProvider>;
};

export default App;
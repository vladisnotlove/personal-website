import React from "react";
import App from "./_app";

const TestPage: React.FC = () => {
	return <App>
		<div>
			404 Page not found
		</div>
	</App>;
};

export default TestPage;

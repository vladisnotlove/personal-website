import "styled-components";

interface IPalette {
	dark?: string,
	main?: string,
	light?: string,
	contrastText?: string,
}

interface ITypography {
	fontSize: string,
	fontFamily?: string,
	fontWeight?: string,
	fontStyle?: string,
	lineHeight?: string,
	letterSpacing?: string,
}

declare module "styled-components" {
	export interface DefaultTheme {
		palette: {
			base: Omit<IPalette, "contrastText">,
			primary: IPalette,
			action: {
				hover?: string,
				disabledOpacity?: string,
			},
			text: {
				primary?: string,
				secondary?: string,
				accent?: string,
			},
			gray: {
				darkest?: string,
				darker?: string,
				dark?: string,
				main?: string,
				light?: string,
				lighter?: string,
				lightest?: string,
			}
		},
		typography: {
			fonts: {
				sans: string,
				mono: string
			}
			base: ITypography,
			h1: ITypography,
			h2: ITypography,
			h3: ITypography,
		},
		animation: {
			fast: string,
			normal: string,
			slow: string,
		}
	}
}
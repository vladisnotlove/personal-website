const size = {
	xs: 480,
	sm: 768,
	md: 1024,
	lg: 1200,
};

const device = {
	xs: `(min-width: ${size.xs}px)`,
	sm: `(min-width: ${size.sm}px)`,
	md: `(min-width: ${size.md}px)`,
	lg: `(min-width: ${size.lg}px)`,
};

const breakpoint = { size, device };

export default breakpoint;
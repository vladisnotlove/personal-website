import { DefaultTheme } from "styled-components";

const theme: DefaultTheme = {
	palette: {
		base: {
			dark: "#F5F5F5",
			main: "#fff",
			light: "#fff",
		},
		text: {
			primary: "#000",
			secondary: "#888",
			accent: "#3364E0",
		},
		primary: {
			dark: "#2852bf",
			main: "#3364E0",
			light: "#5789f1",
			contrastText: "#fff",
		},
		gray: {
			darkest: "#555",
			darker: "#888",
			dark: "#aaa",
			main: "#ccc",
			light: "#ddd",
			lighter: "#e5e5e5",
			lightest: "#f5f5f5",
		},
		action: {
			hover: "rgba(0,0,0, 0.08)",
			disabledOpacity: "0.5"
		}
	},
	typography: {
		fonts: {
			sans: "IBMPlexSans",
			mono: "IBMPlexMono"
		},
		base: {
			fontSize: "1rem",
			fontFamily: "IBMPlexSans",
			lineHeight: "2em",
			letterSpacing: ".05em"
		},
		h1: {
			fontSize: "2.25rem",
			fontWeight: "500",
			lineHeight: "1.5em",
		},
		h2: {
			fontSize: "2.25rem",
			fontWeight: "400",
			lineHeight: "1.5em",
		},
		h3: {
			fontSize: "1.5rem",
		}
	},
	animation: {
		fast: ".125s",
		normal: ".25s",
		slow: ".5s"
	}
};

const brightTheme: DefaultTheme = {
	...theme,

	palette: {
		base: {
			main: theme.palette.primary.main,
		},
		text: {
			primary: "#fff",
			secondary: "#000",
		},
		primary: {
			dark: "#fff",
			main: "#fff",
			light: "#fff",
			contrastText: theme.palette.primary.main,
		},
		gray: {},
		action: {}
	},
};

export default theme;
export {
	brightTheme
};
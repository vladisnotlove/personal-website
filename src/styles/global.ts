import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
	body * {
		font-family: ${props => props.theme.typography.base.fontFamily}, sans-serif;
		font-size: ${props => props.theme.typography.base.fontSize};
		box-sizing: border-box;
	}
`;

export default GlobalStyle;
const { exec } = require("./utils");
const commandLineArgs = require("command-line-args");

const optionDefinitions = [
	{ name: "lang", alias: "l", type: String }
];
const options = commandLineArgs(optionDefinitions);
const command = `formatjs compile src/locales/${options.lang}.extracted.json --out-file src/locales/${options.lang}.json`;

console.log(`> ${command}`);
console.log();

exec(command);
const { exec: _exec } = require("child_process");

const exec = (command) => {
	_exec(command, (error, stdout, stderr) => {
		if (error) {
			console.log(error.message);
			return;
		}
		if (stderr) {
			console.log(stderr);
			return;
		}
		console.log(stdout);
	});
};

module.exports = {
	exec
};

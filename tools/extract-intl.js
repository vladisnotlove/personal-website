const { exec } = require("./utils");
const commandLineArgs = require("command-line-args");

const optionDefinitions = [
	{ name: "lang", alias: "l", type: String }
];
const options = commandLineArgs(optionDefinitions);
const command = `formatjs extract src/**/*.tsx --out-file src/locales/${options.lang}.extracted.json --id-interpolation-pattern '[sha512:contenthash:base64:6]' --throws`;

console.log(`> ${command}`);
console.log();

exec(command);
# Personal website

### Description

This is website about me (Vladislav Nikolaev) as a frontend developer<br>
It contains short info, skills, portfolio and contacts

*Used techs:*<br>
React, Gatsby, Typescript, Styled components, FormatJS

### How to run

    npm run develop

Builds and runs server in dev mode on *http://localhost:8000*

**or**

    npm run build
    npm run serve

The first compiles application<br>
The second serve the production build on *http://localhost:9000*
const path = require("path");

exports.onCreateWebpackConfig = function({ actions }) {
	actions.setWebpackConfig({
		resolve: {
			alias: {
				"components": path.resolve(__dirname, "./src/components/"),
				"styles": path.resolve(__dirname, "./src/styles/"),
				"hooks": path.resolve(__dirname, "./src/hooks/"),
				"data": path.resolve(__dirname, "./src/data/"),
				"types": path.resolve(__dirname, "./src/types/"),
				"images": path.resolve(__dirname, "./src/images/"),
			}
		}
	});
};
module.exports = {
	"env": {
		"browser": true,
		"node": true
	},
	"plugins": [
		"@typescript-eslint",
		"react",
	],
	"extends": [
		"eslint:recommended",
		"plugin:react/recommended",
		"plugin:@typescript-eslint/recommended",
	],
	"parser": "@typescript-eslint/parser",
	"parserOptions": {
		"ecmaVersion": 12,
		"ecmaFeatures": {
			"jsx": true,
		}
	},
	"root": true,
	"rules": {
		"indent": [
			"error",
			"tab",
			{ "SwitchCase": 1 },
		],
		"quotes": [
			"error",
			"double",
		],
		"semi": [
			"error",
			"always",
		],
		"react/prop-types": "off",
		"@typescript-eslint/no-var-requires": 0,
		"object-curly-spacing": ["error", "always"]
	}
};